import axios from "axios";

const fetchFolders = (setDataFn) => {
  axios
    .get(`https://prof.world/api/test_json_files/?token=${process.env.REACT_APP_API_TOKEN}`)
    .then((res) => {
      const { files } = res.data.data;
      setDataFn(files);
    })
    .catch((err) => {
      throw new Error(err);
    });
};

export default fetchFolders;
