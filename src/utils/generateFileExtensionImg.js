const generateFileExtensionImg = (type) => {
  switch (type) {
    case "application/msword":
    case "inode/x-empty":
      return "https://cdn2.vectorstock.com/i/1000x1000/15/86/file-name-extension-doc-type-vector-13931586.jpg";
    case "application/pdf":
      return "https://cdn5.vectorstock.com/i/1000x1000/15/69/file-name-extension-pdf-type-vector-13931569.jpg";
    case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
      return "https://cdn1.vectorstock.com/i/1000x1000/18/30/file-name-extension-xls-type-vector-13931830.jpg";

    case "image/jpeg":
      return "https://cdn4.vectorstock.com/i/1000x1000/15/03/file-name-extension-jpg-type-vector-13931503.jpg";
    case "image/png":
      return "https://cdn4.vectorstock.com/i/1000x1000/13/78/file-name-extension-png-type-vector-13931378.jpg";
    default:
      return "https://fileproinfo.com/images/unknown_file_extension.png";
  }
};
export default generateFileExtensionImg;
