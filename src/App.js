import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Route, Routes, Navigate, Link } from "react-router-dom";
import { CookiesProvider } from "react-cookie";
import { useCookies } from "react-cookie";
import fetchFolders from "./services/fetchFolders";
import { Context } from "./context/index";
import Folders from "./components/Folders/Folders";
import Folder from "./components/Folder/Folder";
import "./App.scss";

function App() {
  const [cookies, setCookie] = useCookies();
  const [folders, setFolders] = useState({});
  const [files, setFiles] = useState([]);
  const [filterOption, setFilterOption] = useState("date");

  useEffect(() => {
    fetchFolders(setFolders);
    if (cookies.filterBy) {
      setFilterOption(cookies.filterBy);
    } else {
      setCookie("filterBy", filterOption, { path: "/" });
    }
  }, []);

  return (
    <Context.Provider
      value={{
        folders,
        setFolders,
        files,
        setFiles,
        filterOption,
        setFilterOption,
      }}
    >
      <CookiesProvider>
        <Router>
          <header className="header">
            <Link to="/">
              <p> My Computer</p>
            </Link>
          </header>
          <div className="app">
            <Routes>
              <Route exact path="/" element={<Folders />} />
              <Route exact path="/:folderName" element={<Folder />} />
              <Route path="*" element={<Navigate to="/" />} />
            </Routes>
          </div>
        </Router>
      </CookiesProvider>
    </Context.Provider>
  );
}

export default App;
