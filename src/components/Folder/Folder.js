import React, { useEffect, useContext, useMemo } from "react";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";
import { useCookies } from "react-cookie";
import { Context } from "../../context/index";
import Filter from "../Filter/Filter";
import generateFileExtensionImg from "../../utils/generateFileExtensionImg";
import "./Folder.scss";

const Folder = () => {
  const [cookies, setCookie] = useCookies();
  const { folderName } = useParams();
  const { folders, files, setFiles, filterOption, setFilterOption } =
    useContext(Context);

  useEffect(() => {
    return () => {
      setFiles([]);
    };
  }, []);

  useEffect(() => {
    folders[folderName] && setFiles(folders[folderName]);
  }, [folderName, folders, setFiles]);

  useEffect(() => {
    setCookie("filterBy", filterOption, { path: "/" });
  }, [filterOption, setCookie]);

  const sorted = useMemo(() => {
    let sortableItems = [...files];
    if (filterOption === "date") {
      sortableItems.sort((a, b) => new Date(b.atime) - new Date(a.atime));
    }
    if (filterOption === "size") {
      sortableItems.sort((a, b) => b.size - a.size);
    }
    if (filterOption === "name") {
      sortableItems.sort((a, b) =>
        a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1
      );
    }
    return sortableItems;
  }, [filterOption, files]);

  return (
    <div className="folderWrapper">
      <Link to="/">
        <button>
          <img
            src="https://www.seekpng.com/png/detail/198-1980787_left-arrow-sign-arrow-back-icon-png.png"
            alt="back-icon"
          />
        </button>
      </Link>

      <Filter onClick={(option) => setFilterOption(option)} />
      <div className="files">
        {files.length > 0 ? (
          <>
            {sorted.map(({ name, type, mtime }) => {
              return (
                <div key={mtime} className="file">
                  <img
                    src={generateFileExtensionImg(type)}
                    alt="file-extension-img"
                  />
                  <p>{name}</p>
                </div>
              );
            })}
          </>
        ) : (
          <div>Empty</div>
        )}
      </div>
    </div>
  );
};

export default Folder;
