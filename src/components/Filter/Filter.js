import React, { useContext } from "react";
import { Context } from "../../context/index";
import cn from "classnames";
import "./Filter.scss";

const Filter = ({ onClick }) => {
  const FILTERS = ["name", "size", "date"];
  const { filterOption } = useContext(Context);
  return (
    <div className="filtersWrapper">
      <p>Filter by:</p>
      <div className="items">
        {FILTERS.map((el) => {
          return (
            <div
              onClick={() => onClick(el)}
              key={el}
              className={cn("filter", el === filterOption && "active")}
            >
              <p>{el}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Filter;
