import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { Context } from "../../context/index";
import "./Folders.scss";

const Folders = () => {
  const { folders } = useContext(Context);
  return (
    <div className="foldersWrapper">
      {Object.keys(folders)?.length > 0 ? (
        <>
          {Object.keys(folders).map((folder) => {
            return (
              <Link key={folder} to={`/${folder}`}>
                <div className="folder">
                  <img
                    src="https://cdn-icons-png.flaticon.com/512/3767/3767084.png"
                    alt="folder"
                  />
                  <p>{folder}</p>
                </div>
              </Link>
            );
          })}
        </>
      ) : (
        <div>Empty</div>
      )}
    </div>
  );
};

export default Folders;
